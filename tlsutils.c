#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tlsutils.h"
#include "util.h"

int is_tls_record(uint8_t *buffer) {
    // Get first 5 bytes
    uint8_t *bytes = malloc(5*sizeof(uint8_t));
    memcpy(bytes, buffer, 5*sizeof(uint8_t));

    // Check first byte for handshake protocol type (0x16)
    if (bytes[0] == 0x16) {
        // Check next two bytes for SSL version
        if(bytes[1] == 0x03 && (bytes[2] >= 0x01 && bytes[2] <= 0x04)) {
            // bytes[3] and bytes[4] make up the record length in bytes.
            return 1;
        }
    }
    return 0;
}

int is_application_data(uint8_t *buffer) {
    // Get first byte
    uint8_t *bytes = malloc(sizeof(uint8_t));
    memcpy(bytes, buffer, sizeof(uint8_t));

    // Check first byte for application data (0x16)
    if (bytes[0] == 0x17) {
        return 1;
    }
    return 0;
}

uint16_t get_application_data_size(uint8_t *buffer) {
    // Get 4th and 5th bytes
    uint8_t high_byte = buffer[3];
    uint8_t low_byte = buffer[4];

    // Calculate size
    return ((high_byte<<8) + low_byte);
}

// Returns the number of bytes of the current TLS handshake message that haven't been read
void update_client_state(proxy_tls_state *tls_state, uint8_t *buffer, int32_t bytes_read, uint16_t stream_id) {
    // Get current server state and set initial value of bytes read
    uint8_t *current_state = &(tls_state->client_state);
    uint32_t *unread_tls_packet_size = &(tls_state->cli_unread_tls_packet_size);
    int32_t init_bytes_read = bytes_read;
    uint8_t *initial_buffer = buffer;
    uint32_t prev_packet_size = 0;
    uint32_t packet_size = 0;

    if (bytes_read <= *unread_tls_packet_size) {
        *unread_tls_packet_size = -(bytes_read - *unread_tls_packet_size);
        return;
    }

    while (bytes_read > 0) {
        // Reset packet_size to 0
        packet_size = 0;

        switch(*current_state) {
            case 0x00:
                // Check is TLS record
                if (is_tls_record(buffer)) {
                    // Get 6th byte
                    uint8_t *byte = malloc(sizeof(uint8_t));
                    memcpy(byte, buffer+5, sizeof(uint8_t));

                    if (*byte == 0x01) {
                        // Update to ClientHello
                        *current_state = 0x01;

                        // Get size of packet
                        uint8_t high_byte = buffer[6];
                        uint8_t mid_byte = buffer[7];
                        uint8_t low_byte = buffer[8];
                        packet_size += high_byte << 16;
                        packet_size += mid_byte << 8;
                        packet_size += low_byte + 9;
                        prev_packet_size = packet_size;
                        print_client_state(tls_state, packet_size, stream_id);
                    }
                }
                break;
            case 0x01:
                // Check is TLS record
                if (is_tls_record(buffer)) {
                    // Get 6th byte
                    uint8_t *byte = malloc(sizeof(uint8_t));
                    memcpy(byte, buffer+5, sizeof(uint8_t));

                    if (*byte == 0x10) {
                        // Update to ClientKeyExchange
                        *current_state = 0x02;

                        // Get size of packet
                        uint8_t high_byte = buffer[6];
                        uint8_t mid_byte = buffer[7];
                        uint8_t low_byte = buffer[8];
                        packet_size += high_byte << 16;
                        packet_size += mid_byte << 8;
                        packet_size += low_byte + 9;
                        prev_packet_size = packet_size;
                        print_client_state(tls_state, packet_size, stream_id);
                    }
                }
                break;
            case 0x02:
                // Check if packet is just after ClientKeyExchange
                if (prev_packet_size > 0) {
                    buffer = initial_buffer + (init_bytes_read - bytes_read);
                } else if (*unread_tls_packet_size > 0) {
                    buffer = initial_buffer + *unread_tls_packet_size;
                    bytes_read -= *unread_tls_packet_size;
                    *unread_tls_packet_size = 0;
                }

                // Check first byte
                if (*buffer == 0x14) {
                    // Update to ClientChangeCipherSpec
                    *current_state = 0x03;

                    // Get size of packet
                    uint8_t high_byte = buffer[3];
                    uint8_t low_byte = buffer[4];
                    packet_size += high_byte << 8;
                    packet_size += low_byte + 5;
                    prev_packet_size = packet_size;
                    print_client_state(tls_state, packet_size, stream_id);
                }
                break;
            case 0x03:
                // Check if packet is just after ClientChangeCipherSpec
                if (prev_packet_size > 0) {
                    buffer = initial_buffer + (init_bytes_read - bytes_read);
                } else if (*unread_tls_packet_size > 0) {
                    buffer = initial_buffer + *unread_tls_packet_size;
                    bytes_read -= *unread_tls_packet_size;
                    *unread_tls_packet_size = 0;
                }

                // Check is TLS record
                if (is_tls_record(buffer)) {
                    // Update to ClientHandshakeFinished
                    *current_state = 0x04;

                    // Get size of packet
                    uint8_t high_byte = buffer[3];
                    uint8_t low_byte = buffer[4];
                    packet_size += high_byte << 8;
                    packet_size += low_byte + 5;
                    prev_packet_size = packet_size;
                    print_client_state(tls_state, packet_size, stream_id);
                }
            case 0x04:
                // Check first byte
                if (*buffer == 0x15) {
                    // Update to ClientCloseNotify
                    *current_state = 0x05;

                    // Get size of packet
                    uint8_t high_byte = buffer[3];
                    uint8_t low_byte = buffer[4];
                    packet_size += high_byte << 8;
                    packet_size += low_byte + 5;
                    prev_packet_size = packet_size;
                    print_client_state(tls_state, packet_size, stream_id);
                }
                break;
            default:
                printf("Invalid client TLS state: %hhu\n", *current_state);
                break;
        }

        // Decrement bytes read by packet size
        bytes_read -= packet_size;

        // DEBUG_MSG(DEBUG_PROXY, "First 10 bytes: ");DEBUG_BYTES(DEBUG_PROXY, buffer, 10);
    }
    
    if (bytes_read < 0) {
        *unread_tls_packet_size = -bytes_read;
    }
}

void print_client_state(proxy_tls_state *tls_state, uint32_t packet_size, uint16_t stream_id) {
    // Get current client state
    uint8_t *current_state = &(tls_state->client_state);

    switch(*current_state) {
        case 0x01:
            DEBUG_MSG(DEBUG_PROXY, "PROXY (id %d): TLS ClientHello of size %u bytes\n", stream_id, packet_size);
            break;
        case 0x02:
            DEBUG_MSG(DEBUG_PROXY, "PROXY(id %d): TLS ClientKeyExchange of size %u bytes\n", stream_id, packet_size);
            break;
        case 0x03:
            DEBUG_MSG(DEBUG_PROXY, "PROXY(id %d): TLS ClientChangeCipherSpec of size %u bytes\n", stream_id, packet_size);
            break;
        case 0x04:
            DEBUG_MSG(DEBUG_PROXY, "PROXY(id %d): TLS ClientHandshakeFinished of size %u bytes\n", stream_id, packet_size);
            break;
        case 0x05:
            DEBUG_MSG(DEBUG_PROXY, "PROXY(id %d): TLS ClientCloseNotify of size %u bytes\n", stream_id, packet_size);
    }
}

// Returns the number of bytes of the current TLS handshake message that haven't been read
void update_server_state(proxy_tls_state *tls_state, uint8_t *buffer, int32_t bytes_read, uint16_t stream_id) {
    // Get current server state and set initial value of bytes read
    uint8_t *current_state = &(tls_state->server_state);
    uint32_t *unread_tls_packet_size = &(tls_state->srv_unread_tls_packet_size);
    int32_t init_bytes_read = bytes_read;
    uint8_t *initial_buffer = buffer;
    uint32_t prev_packet_size = 0;
    uint32_t packet_size = 0;

    if (bytes_read <= *unread_tls_packet_size) {
        *unread_tls_packet_size = -(bytes_read - *unread_tls_packet_size);
        return;
    }

    while (bytes_read > 0) {
        // Reset packet_size to 0
        packet_size = 0;

        switch(*current_state) {
            case 0x00:
                // Check is TLS record
                if (is_tls_record(buffer)) {
                    // Get 6th byte
                    uint8_t *byte = malloc(sizeof(uint8_t));
                    memcpy(byte, buffer+5, sizeof(uint8_t));

                    if (*byte == 0x02) {
                        // Update to ServerHello
                        *current_state = 0x01;

                        // Get size of packet
                        uint8_t high_byte = buffer[6];
                        uint8_t mid_byte = buffer[7];
                        uint8_t low_byte = buffer[8];
                        packet_size += high_byte << 16;
                        packet_size += mid_byte << 8;
                        packet_size += low_byte + 9;
                        prev_packet_size = packet_size;
                        print_server_state(tls_state, packet_size, stream_id);
                    }
                }
                break;
            case 0x01:
                // Check if packet is just after ServerHello
                if (prev_packet_size > 0) {
                    buffer = initial_buffer + (init_bytes_read - bytes_read);
                } else if (*unread_tls_packet_size > 0) {
                    buffer = initial_buffer + *unread_tls_packet_size;
                    bytes_read -= *unread_tls_packet_size;
                    *unread_tls_packet_size = 0;
                }

                // Check is TLS record
                if (is_tls_record(buffer)) {
                    // Get 6th byte
                    uint8_t *byte = malloc(sizeof(uint8_t));
                    memcpy(byte, buffer+5, sizeof(uint8_t));

                    if (*byte == 0x0B) {
                        // Update to ServerCertificate
                        *current_state = 0x02;

                        // Get size of packet
                        uint8_t high_byte = buffer[6];
                        uint8_t mid_byte = buffer[7];
                        uint8_t low_byte = buffer[8];
                        packet_size += high_byte << 16;
                        packet_size += mid_byte << 8;
                        packet_size += low_byte + 9;
                        prev_packet_size = packet_size;
                        print_server_state(tls_state, packet_size, stream_id);
                    }
                }
                break;
            case 0x02:
                // Check if packet is just after ServerCertificate
                if (prev_packet_size > 0) {
                    buffer = initial_buffer + (init_bytes_read - bytes_read);
                } else if (*unread_tls_packet_size > 0) {
                    buffer = initial_buffer + *unread_tls_packet_size;
                    bytes_read -= *unread_tls_packet_size;
                    *unread_tls_packet_size = 0;
                }

                // Check is TLS record
                if (is_tls_record(buffer)) {
                    // Get 6th byte
                    uint8_t *byte = malloc(sizeof(uint8_t));
                    memcpy(byte, buffer+5, sizeof(uint8_t));

                    if (*byte == 0x0C) {
                        // Update to ServerKeyExchange
                        *current_state = 0x03;

                        // Get size of packet
                        uint8_t high_byte = buffer[6];
                        uint8_t mid_byte = buffer[7];
                        uint8_t low_byte = buffer[8];
                        packet_size += high_byte << 16;
                        packet_size += mid_byte << 8;
                        packet_size += low_byte + 9;
                        prev_packet_size = packet_size;
                        print_server_state(tls_state, packet_size, stream_id);
                    }
                }
                break;
            case 0x03:
                // Check if packet is just after ServerKeyExchange
                if (prev_packet_size > 0) {
                    buffer = initial_buffer + (init_bytes_read - bytes_read);
                } else if (*unread_tls_packet_size > 0) {
                    buffer = initial_buffer + *unread_tls_packet_size;
                    bytes_read -= *unread_tls_packet_size;
                    *unread_tls_packet_size = 0;
                }

                // Check is TLS record
                if (is_tls_record(buffer)) {
                    // Get 6th byte
                    uint8_t *byte = malloc(sizeof(uint8_t));
                    memcpy(byte, buffer+5, sizeof(uint8_t));

                    if (*byte == 0x0E) {
                        // Update to ServerHelloDone
                        *current_state = 0x04;

                        // Get size of packet
                        uint8_t high_byte = buffer[6];
                        uint8_t mid_byte = buffer[7];
                        uint8_t low_byte = buffer[8];
                        packet_size += high_byte << 16;
                        packet_size += mid_byte << 8;
                        packet_size += low_byte + 9;
                        prev_packet_size = packet_size;
                        print_server_state(tls_state, packet_size, stream_id);
                    }
                }
            case 0x04:
                if (*unread_tls_packet_size > 0) {
                    buffer = initial_buffer + *unread_tls_packet_size;
                    bytes_read -= *unread_tls_packet_size;
                    *unread_tls_packet_size = 0;
                }

                // Check first byte
                if (*buffer == 0x14) {
                    // Update to ServerChangeCipherSpec
                    *current_state = 0x05;

                    // Get size of packet
                    uint8_t high_byte = buffer[3];
                    uint8_t low_byte = buffer[4];
                    packet_size += high_byte << 8;
                    packet_size += low_byte + 5;
                    prev_packet_size = packet_size;
                    print_server_state(tls_state, packet_size, stream_id);
                }
                break;
            case 0x05:
                // Check if packet is just after ServerChangeCipherSpec
                if (prev_packet_size > 0) {
                    buffer = initial_buffer + (init_bytes_read - bytes_read);
                } else if (*unread_tls_packet_size > 0) {
                    buffer = initial_buffer + *unread_tls_packet_size;
                    bytes_read -= *unread_tls_packet_size;
                    *unread_tls_packet_size = 0;
                }

                // Check is TLS record
                if (is_tls_record(buffer)) {
                    // Update to ServerHandshakeFinished
                    *current_state = 0x06;

                    // Get size of packet
                    uint8_t high_byte = buffer[3];
                    uint8_t low_byte = buffer[4];
                    packet_size += high_byte << 8;
                    packet_size += low_byte + 5;
                    prev_packet_size = packet_size;
                    print_server_state(tls_state, packet_size, stream_id);
                }
                break;
            default:
                printf("Invalid server TLS state: %hhu\n", *current_state);
                break;
        }

        // Decrement bytes read by packet size
        bytes_read -= packet_size;

        // DEBUG_MSG(DEBUG_PROXY, "First 10 bytes: ");DEBUG_BYTES(DEBUG_PROXY, buffer, 10);
    }
    
    if (bytes_read < 0) {
        *unread_tls_packet_size = -bytes_read;
    }
}

void print_server_state(proxy_tls_state *tls_state, uint32_t packet_size, uint16_t stream_id) {
    // Get current server state
    uint8_t *current_state = &(tls_state->server_state);

    switch(*current_state) {
        case 0x01:
            DEBUG_MSG(DEBUG_PROXY, "PROXY (id %d): TLS ServerHello of size %u bytes\n", stream_id, packet_size);
            break;
        case 0x02:
            DEBUG_MSG(DEBUG_PROXY, "PROXY(id %d): TLS ServerCertificate of size %u bytes\n", stream_id, packet_size);
            break;
        case 0x03:
            DEBUG_MSG(DEBUG_PROXY, "PROXY(id %d): TLS ServerKeyExchange of size %u bytes\n", stream_id, packet_size);
            break;
        case 0x04:
            DEBUG_MSG(DEBUG_PROXY, "PROXY(id %d): TLS ServerHelloDone of size %u bytes\n", stream_id, packet_size);
            break;
        case 0x05:
            DEBUG_MSG(DEBUG_PROXY, "PROXY(id %d): TLS ServerChangeCipherSpec of size %u bytes\n", stream_id, packet_size);
            break;
        case 0x06:
            DEBUG_MSG(DEBUG_PROXY, "PROXY(id %d): TLS ServerHandshakeFinished of size %u bytes\n", stream_id, packet_size);
    }
}