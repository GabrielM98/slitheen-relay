#ifndef TLSUTILS_H
#define TLSUTILS_H

#include <stddef.h>
#include <stdint.h>

typedef struct proxy_tls_state_st {
    uint8_t client_state;
    uint8_t server_state;
    uint32_t cli_unread_tls_packet_size;
    uint32_t srv_unread_tls_packet_size;
} proxy_tls_state;

int is_tls_record(uint8_t *buffer);
int is_application_data(uint8_t *buffer);
uint16_t get_application_data_size(uint8_t *buffer);

void update_client_state(proxy_tls_state *tls_state, uint8_t *buffer, int32_t bytes_read, uint16_t stream_id);
void print_client_state(proxy_tls_state *tls_state, uint32_t packet_size, uint16_t stream_id);

void update_server_state(proxy_tls_state *tls_state, uint8_t *buffer, int32_t bytes_read, uint16_t stream_id);
void print_server_state(proxy_tls_state *tls_state, uint32_t packet_size, uint16_t stream_id);

#endif /* TLSUTILS_H */